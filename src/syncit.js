/*
*	 syncit.js - queue asynchronous calls and call them sequentially
*  Copyright (C) 2018, Vladimir Sukhov, v. 0.01b
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var syncit = new Syncit(); // global var for window;

/*
	Syncable - object accepted to syncit as a function parameter,
	see documentation on examples
*/
function Syncable(func, params, returnable, expect)
{
	if(typeof(func) !== "function")
	{
		throw new TypeError("1st argument should be a valid function");
		return;
	}

	if(typeof(returnable) !== typeof(true))
	{
		throw new TypeError("2nd argument should be a typeof boolean");
		return;
	}

	if(true === returnable)
	{
		const et = typeof(expect);
		if(et !== "object" && et !== "function" && et !== "symbol" &&
		   et !== "symbol" && et !== "string" && et !== "number" && et !== "boolean"
		 	 && et !== "undefined")
		{
			throw new TypeError("3rd argument should be of a known type");
			return;
		}
	}
	this.f = func;
	this.r = returnable;
	this.e = expect;
	this.genericname = "syncable";
	this.p = params;
}


function Syncit(debug)
{
	this.callstack = new Array();
	this.tData;
	this.q = 0; // queue
	this.e = 0;
	this.debug = !debug ? false : true;
	this.cback;
}

Syncit.prototype.addToStack = function(syncable)
{
	if(syncable.hasOwnProperty("genericname") && syncable.genericname == 'syncable')
	{
		this.q++;
		if(!syncable.r)
		{
			syncable.f = this.wrap(syncable.f, syncable.p);
		}
		this.callstack.push(syncable);
	}
	else
	{
		throw new TypeError("Only typeof Syncable can be added to a stack");
		return;
	}
}

Syncit.prototype.wrap = function(func, args)
{
	return function(func, args)
	{
		Syncit.prototype.callback(func(args))	;
	};

}

Syncit.prototype.run = function(i)
{
	var x = i;
	if(!x){x = 0;}
	this.e++;

	var newParams = [];
	for (var i = 0; i < this.callstack[x].p.length; i++)
	{
		if(this.callstack[x].p[i] === "syncit.result" && x > 0)
		{
			newParams.push(window.syncit.tData);
			continue;
		}
		newParams.push(this.callstack[x].p[i]);
	}

	var callable = this.callstack[x].f.apply(null,newParams);
	callable;

}

Syncit.prototype.insertResult = function(params)
{
	var newParams = [];
	for (var i = 0; i < params.length; i++)
	{
		if(params[i] === "syncit.result")
		{
			newParams.push(window.syncit.tData);
			continue;
		}
		newParams.push(params[i]);
	}
}

Syncit.prototype.callback = function(data)
{

	if(window.syncit.callstack[window.syncit.e - 1].e !== typeof(data))
	{
		throw new TypeError("Was expecting to receive: " + window.syncit.callstack[window.syncit.e -1].e + ", got: " + typeof(data));
		return;
	}

	window.syncit.tData = data;

	if(window.syncit.q === window.syncit.e)
	{
		if(window.syncit.debug)
		{
			console.log(data);console.log("done");
		}
		window.syncit.cback(data);
		if(!window.syncit.debug)
			window.syncit.clean();
		return;
	}
	if(window.syncit.debug)
		console.log(data);

	window.syncit.cback(data);
	window.syncit.run(window.syncit.e);
}

Syncit.prototype.clean = function()
{
	window.syncit.callstack = new Array();
	window.syncit.tData;
	window.syncit.q = 0; // queue
	window.syncit.e = 0;
}
