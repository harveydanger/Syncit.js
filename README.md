# Syncit.js

## Description
Tired from Promises and asynchronous chains? Syncit.js delivers a proper queue of tasks execution with straight through parameters passing. No dependencies, pure vanilla JS (ES2016) standard.
## USAGE
### Import
```
<script src="syncit.js" charset="utf-8"></script>
```
## Example usage

#### 1. Assign your own callback function for results:

```
syncit.cback = data => document.write(data + "<br>");
```
#### 2. Create new Syncable task -> new Syncable(function, [arguments], returnable, type)
Yes, it is an attempt for at least control the type of return.
```
  // Function 1 -> with Timeout
      const xf = function(param, cb)
      {
        var result = param + 5;
        setTimeout(function() {
          cb(result);
        },2000);
      }

      // Function 2 - with plain callback
      const yf = function(param, cb)
      {
        var result = param + 1;
         cb(result);
      }
      const x = new Syncable(xf,[5,syncit.callback], true, "number");
      const y = new Syncable(yf,["syncit.result", syncit.callback], true, "number");
```

* Use "syncit.result" as a pass through param from the previous queue member
* Use syncit.callback internal method for callback redirection

#### 3. Add Syncables to the execution queue
```
syncit.addToStack(x);
syncit.addToStack(y);
```

#### 4. Run Syncit
```
syncit.run();
```

#### Clean if you need to add more queues
```
syncit.clean();
```

Full example in the source.
**Copyright (c) Vladimir Sukhov 2018**
**GPL 3.0 License**
